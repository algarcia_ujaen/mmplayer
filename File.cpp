/**
 * @file File.cpp
 * @brief Implementation of the methods of the File class
 * @author algarcia
 * @date 9/2/22
 */

#include <iostream>
#include "File.h"

/**
 * Copy constructor
 * @param orig Object to be copied
 * @post The new object keeps a copy of the attributes of the original object
 * @throw std::bad_alloc If there is any problem allocating memory
 */
File::File ( const File &orig ): _name ( orig._name ), _size ( orig._size )
{  if ( orig._fileType != nullptr )
   {  _fileType = new std::string ( *orig._fileType );
   }
}

/**
 * Parameterized constructor
 * @param name Name of the new file
 * @param fT Family name of the new file
 * @throw std::bad_alloc If there is any problem allocating memory
 */
File::File ( const std::string &name, const std::string &fT ) try : _name ( name )
/*Object allocated in the heap*/            , _fileType ( new std::string ( fT ) )
{ }
catch ( std::bad_alloc &e )
{  std::cerr << "The file could not be created" << std::endl;
   throw e;
}

/**
 * Destructor
 */
File::~File ()
{  if ( _fileType != nullptr )
   {  delete _fileType;
      _fileType = nullptr;
   }

   _author = nullptr;
}

/**
 * Observer of the file name
 * @return The name of the file
 */
const std::string &File::getName() const
{  return _name;
}

/**
 * Modifier of the file newName
 * @param newName New newName of the file
 * @pre The newName has to be a valid one
 * @post The newName of the file is changed to the value received as parameter
 */
void File::setName(const std::string &newName)
{  _name = newName;
}

/**
 * "Less than" operator, based on the file size
 * @param other File to be compared with
 * @retval true If the size of the current file is less than or equal to the
 *              size of the file that is received as parameter
 * @retval false If the size of the current file is greater than the size of the
 *               file that is received as parameter
 */
bool File::operator<= ( const File &other )
{  bool returnValue = true;

   if ( _size > other._size )
   {  returnValue = false;
   }

   return returnValue;
}

/**
 * "Equal to" operator
 * @param other File to be compared with
 * @retval true If the files have the same name, file type and size
 * @retval false If the name, file type or size of the files are different
 */
bool File::operator== ( const File &other )
{  bool returnValue = true;

   if ( _fileType != nullptr )
   {  if ( other._fileType == nullptr )
      {  returnValue = false;
      }
      else
      {  if ( *_fileType != *other._fileType )
         {  returnValue = false;
         }
      }
   }
   else
   {  if ( other._fileType != nullptr )
      {  returnValue = false;
      }
   }

   if ( ( _name != other._name ) || ( _size != other._size ) )
   {  returnValue = false;
   }

   return returnValue;
}

/**
 * "Post increment" operator
 * @param param Discarded
 * @post The size of the file is incremented in one unit
 * @return A reference to the same file
 */
File &File::operator++ ( int param )
{  _size++;
   return *this;
}

/**
 * Observer for the author of the work in the file
 * @return A pointer to the author of the work in the file
 * @retval nullptr If there is no author for the work in the file
 */
Artist *File::getAuthor () const
{  return _author;
}

/**
 * Assigns a new author for the work in the file
 * @param author Pointer to the author of the work in the file. Any value
 *        (including nullptr) is accepted.
 * @note No check is performed on the parameter
 * @post The author of the work in the file is changed
 */
void File::setAuthor ( Artist *author )
{  _author = author;
}
