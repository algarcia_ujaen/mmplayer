#include <iostream>
#include "PlayList.h"
#include "VideoFile.h"

// Parameter passing by copy. The copy constructor is called
// void show ( File paramF )
// Parameter passing by reference. No copy constructor is called
void show ( File &paramF )
{  std::cout << "The name of file xxx is "
        << paramF.getName() << std::endl;
}

int main()
{  int integer01 = 0;
   File file01;   // Calls the default constructor
   File file02 ( "kitty.avi", "videos" );   // Calls the parameterized constructor

   File file03 ( file02 );   // Calls the copy constructor

   // Creates an object in the heap (default constructor)
   File *pFile01 = new File;
   // Creates an object in the heap (parameterized constructor)
   File *pFile02 = new File ( "anotherKitty.avi", "videos" );
   // Creates an object in the heap (copy constructor)
   File *pFile03 = new File ( file01 );
   // Creates an object in the heap (copy constructor)
   File *pFile04 = new File ( *pFile01 );

   // Vector of objects in the heap (calls the default constructor 3 times)
   File *vector = new File [3];

   // Vector of objects in the stack (calls the default constructor 3 times)
   File vector2 [3];

   show (file03);

   std::cout << "The name of file 1 is "
        << pFile03->getName() << std::endl   // Using the pointer to call a method
        << "The name of file 2 is "
        << (*pFile02).getName() << std::endl;   // Using the pointer to call a method

   file01++;

   if ( file01 <= file02 )
   {  std::cout << "file01 is less than" << std::endl;
   }
   else
   {  std::cout << "file01 is greater than" << std::endl;
   }

   PlayList p1;
   try
   {  p1.addFile ( &file01 );
      p1.addFile ( pFile03 );
   }
   catch ( std::invalid_argument &e )
   {  std::cerr << e.what() << std::endl;
   }
   catch ( std::runtime_error &e )
   {  std::cerr << e.what() << std::endl;
   }

   // Free the resources allocated in the heap
   delete pFile01;
   delete pFile02;
   delete pFile03;
   delete pFile04;
   delete [] vector;
   // It is VERY ADVISABLE to assign nullptr to the pointers
   vector = pFile04 = pFile03 = pFile02 = pFile01 = nullptr;

   Artist a1;

   file01.setAuthor ( &a1 );

   VideoFile vf1;
   return 0;
}
