/**
 * @file VideoFile.h
 * @brief Declaration of the VideoFile class
 * @author algarcia
 * @date 30/3/22
 */

#ifndef MMPLAYER_VIDEOFILE_H
#define MMPLAYER_VIDEOFILE_H

#include "File.h"

/**
 * The objects of this class will represent video files
 */
class VideoFile: public File
{
   private:
      int _width = 0;   ///< Width of the video image (in pixels)
      int _height = 0;   ///< Height of the video image (in pixels)
      float _fps = 0;   ///< Frames per second of the video

   public:
      VideoFile();
      virtual ~VideoFile();
};

#endif //MMPLAYER_VIDEOFILE_H
