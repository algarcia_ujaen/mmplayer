/**
 * @file PlayList.h
 * @brief Declaration of the PlayList class
 * @author algarcia
 * @date 11/3/22
 */

#ifndef MMPLAYER_PLAYLIST_H
#define MMPLAYER_PLAYLIST_H

#include "File.h"

/**
 * The objects of this class store playlists made of several files
 */
class PlayList
{
   public:
      static const int MAX_PLAYLIST_SIZE = 20;

   private:
      int _numFiles = 0;
      File *_files[MAX_PLAYLIST_SIZE];

   public:
      PlayList ();
      virtual ~PlayList ();
      File *getFile ( int which ) const;
      void addFile ( File *pF );
};

#endif //MMPLAYER_PLAYLIST_H
