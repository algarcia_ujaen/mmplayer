/**
 * @file Artist.cpp
 * @brief Implementation of the methods from the Artist class
 * @author algarcia
 * @date 11/3/22
 */

#include "Artist.h"

/**
 * Destructor
 */
Artist::~Artist ()
{ }

/**
 * Observer for the name of the artist
 * @return The name of the artist
 */
std::string Artist::getName () const
{  return _name;
}

/**
 * Modifies the pName of the artist
 * @param pName New pName of the artist
 * @post The pName of the artist changes to the parameter value
 */
void Artist::setName ( const std::string &pName )
{  _name = pName;
}

/**
 * Observer for the artist country
 * @return The country the artist comes from
 */
const std::string &Artist::getCountry () const
{  return _country;
}

/**
 * Modifies the pCountry the artist comes from
 * @param pCountry New pCountry name. No check is performed on the value
 * @post The pCountry the artist comes from is changed to the new value
 */
void Artist::setCountry ( const std::string &pCountry )
{  _country = pCountry;
}

/**
 * Observer for the birthdate of the artist
 * @return The birthdate of the artist as a number with format YYYYMMDD
 */
int Artist::getBirthDate () const
{  return _birthDate;
}

/**
 * Modifies the artist birthdate
 * @param pBDate New birthdate of the artist. Must have format YYYYMMDD
 * @pre pBDate stores a valid date
 * @post The birthdate of the artist changes to the new value
 */
void Artist::setBirthDate ( int pBDate )
{  _birthDate = pBDate;
}

