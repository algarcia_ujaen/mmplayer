/**
 * @file File.h
 * @brief Declaration of the File class
 * @author algarcia
 * @date 9/2/22
 */

#ifndef MMPLAYER_FILE_H
#define MMPLAYER_FILE_H

#include <string>

#include "Artist.h"

/**
 * The objects of this class store information related to multimedia files
 */
class File
{
   private:
      std::string _name = "";   ///< Name of the file
      std::string *_fileType = nullptr;   ///< Type of the file
      unsigned long _size = 0;   ///< Size of the file in bytes
      Artist *_author = nullptr;   ///< Link to the author of the work in the file

   public:
      File () = default;  ///< Default constructor
      File ( const File &orig );  // Copy constructor
      File ( const std::string &name, const std::string &fT );  // Parameterized
      ~File();  // Destructor
      const std::string &getName() const;
      void setName(const std::string &newName);
      bool operator<= ( const File& other );
      bool operator== ( const File& other );
      File& operator++ ( int param );
      Artist *getAuthor () const;
      void setAuthor ( Artist *author );
};


#endif //MMPLAYER_FILE_H
