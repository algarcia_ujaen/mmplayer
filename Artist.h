/**
 * @file Artist.h
 * @brief Declaration of the class Artist
 * @author algarcia
 * @date 11/3/22
 */

#ifndef MMPLAYER_ARTIST_H
#define MMPLAYER_ARTIST_H

#include <string>

/**
 * The objects of this class store information of an artist
 */
class Artist
{
   private:
      std::string _name = "";   ///< Name of the artist
      std::string _country = "";   ///< Country where the artist comes from
      int _birthDate = 0;   ///< Birthdate of the artist. Format YYYYMMDD

   public:
      Artist () = default;
      ~Artist();
      std::string getName () const;
      void setName ( const std::string &pName );
      const std::string &getCountry () const;
      void setCountry ( const std::string &pCountry );
      int getBirthDate () const;
      void setBirthDate ( int pBDate );
};

#endif //MMPLAYER_ARTIST_H
