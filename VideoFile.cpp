/**
 * @file VideoFile.h
 * @brief Implementation of the methods from the VideoFile class
 * @author algarcia
 * @date 30/3/22
 */

#include "VideoFile.h"

/**
 * Default constructor
 */
VideoFile::VideoFile (): File()
{}

/**
 * Destructor
 */
VideoFile::~VideoFile ()
{}
