/**
 * @file PlayList.h
 * @brief Implementation of the methods from the PlayList class
 * @author algarcia
 * @date 11/3/22
 */

#include "PlayList.h"

#include <stdexcept>

/**
 * Default constructor
 */
PlayList::PlayList ()
{  for ( int i = 0;i < MAX_PLAYLIST_SIZE; i++ )
   {  _files[i] = nullptr;
   }
}

/**
 * Destructor
 */
PlayList::~PlayList ()
{  for ( int i = 0; i < _numFiles; i++ )
   {  _files [i] = nullptr;
   }
}

/**
 * Observer for the files of the playlist
 * @param which Index of the file to be observed
 * @return The memory address of a file of the playlist
 * @retval nullptr If there is no file in the playlist for the given index
 * @throw std::invalid_parameter If the value of which is not in the range
 *        [0, number of files - 1]
 */
File *PlayList::getFile ( int which ) const
{  if ( ( which < 0 ) || ( which >= _numFiles ) )
   {  throw std::invalid_argument ( "PlayList::getFile: no file" );
   }

   return _files[which];
}

/**
 * Adds a file to the playlist
 * @param pF Memory address of the new file
 * @post A new file is added to the playlist
 * @throw std::invalid_argument If pF does not point to a File
 * @throw std::runtime_error If there is no room for the new file
 */
void PlayList::addFile ( File *pF )
{  if ( _numFiles == MAX_PLAYLIST_SIZE )
   {  throw std::runtime_error ( "PlayList::addFile: no room" );
   }

   if ( pF == nullptr )
   {  throw std::invalid_argument ( "PlayList::addFile: no file" );
   }

   _files[_numFiles] = pF;
   _numFiles++;
}

